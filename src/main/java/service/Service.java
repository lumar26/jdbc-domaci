package service;

import database.Repository;
import domain.Address;
import domain.BusinessPartner;

import java.sql.SQLException;
import java.util.List;

public class Service {
    private Repository repo;

    public Service() {
        try {
            this.repo = Repository.getInstance();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda za cuvanje liste partnera u bazi bez obzira na njihov tip
     *
     * @param partners lista poslovnih partnera, bilo fizickih lica, bilo pravnih
     */
    public void saveBusinessPartners(List<BusinessPartner> partners) throws SQLException {
        repo.getConnection();
        repo.saveBusinessPartners(partners);
        repo.commit();
    }

    public void saveAddresses(List<Address> addresses) throws Exception {
        repo.getConnection();
        try {
            repo.saveAddresses(addresses);
            repo.commit();
        } catch (SQLException e) {
            repo.rollback();
            e.printStackTrace();
            throw new Exception("Could not save addresses to database");
        }
    }

    public void saveOrUpdatePartner(BusinessPartner partner) throws Exception {
        repo.getConnection();
        try {
            repo.saveOrUpdate(partner);
            repo.commit();
        } catch (SQLException e) {
            repo.rollback();
            e.printStackTrace();
            throw new Exception("Could not save or update partner");
        }
    }

}
