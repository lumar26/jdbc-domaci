package util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DatabaseProperties {
    private static Properties props;
    private static DatabaseProperties instance;

    private DatabaseProperties() {
        props = new Properties();
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("database.properties");
        try {
//            props.load(new FileInputStream("database.properties"));
            props.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static DatabaseProperties getProps() {
        if (instance == null) instance = new DatabaseProperties();
        return instance;
    }

    public String getDatabaseUrl(){
        return props.getProperty("url");
    }

    public String getUser(){
        return props.getProperty("user");
    }

    public String getPassword(){
        return props.getProperty("password");
    }

    public String getConnectionFactoryClassName(){
        return props.getProperty("connection_factory_class");
    }
}
