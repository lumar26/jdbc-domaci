package database;

import domain.Address;
import domain.BusinessPartner;
import domain.concrete.LegalEntity;
import domain.concrete.NaturalEntity;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;
import util.DatabaseProperties;

import java.sql.*;
import java.util.List;

public class Repository {
    private static Repository instance;
    private PoolDataSource ds;
    private Connection connection;

    private Repository() throws SQLException {
        initializeDataSource();
    }

    private void initializeDataSource() throws SQLException {
        ds = PoolDataSourceFactory.getPoolDataSource();
        ds.setConnectionFactoryClassName(DatabaseProperties.getProps().getConnectionFactoryClassName());
        ds.setURL(DatabaseProperties.getProps().getDatabaseUrl());
        ds.setPassword(DatabaseProperties.getProps().getPassword());
        ds.setUser(DatabaseProperties.getProps().getUser());
        ds.setAbandonedConnectionTimeout(3000);
        ds.setInitialPoolSize(31);
        ds.setMaxPoolSize(100);
    }

    public void getConnection() throws SQLException {
        connection = ds.getConnection();
        connection.setAutoCommit(false);
    }

    public static Repository getInstance() throws SQLException {
        if (instance == null) instance = new Repository();
        return instance;
    }

    public void saveBusinessPartners(List<BusinessPartner> partners) throws SQLException {
        String insert = "insert into business_partner (name, address_id) values (?, ?)";
        String insertLegalEntity = "insert into legal_entity (lei, founding_year, id) values (?, ?, ?)";
        String insertNaturalEntity = "insert into natural_entity (personal_id, firstname, lastname, id) values (?, ?, ?, ?)";
        PreparedStatement stmt = connection.prepareStatement(insert, PreparedStatement.RETURN_GENERATED_KEYS);
        PreparedStatement stmt2 = connection.prepareStatement(insertLegalEntity);
        PreparedStatement stmt3 = connection.prepareStatement(insertNaturalEntity);

        for (BusinessPartner p : partners) {
            /*cuvanje u tabeli business_partner informacija koje su zajednicke za sve*/
            stmt.setString(1, p.getName());
            stmt.setInt(2, p.getAddress().getId()); // pretpostavka je da adrese vec postoje u bazi

            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            p.setId(rs.getInt(1));
            rs.close();

            /*cuvanje konkretnih podataka u konkretnim tabelama*/
            saveIfLegalEntity(p, stmt2);
            saveIfNaturalEntity(p, stmt3);
        }
        stmt.close();
        stmt2.close();
        stmt3.close();
    }

    private void saveIfLegalEntity(BusinessPartner p, PreparedStatement stmt2) throws SQLException {
        if (!(p instanceof LegalEntity))
            return;
        LegalEntity temp = (LegalEntity) p;
        stmt2.setString(1, temp.getLEI());
        stmt2.setInt(2, temp.getFoundingYear());
        stmt2.setInt(3, temp.getId());

        stmt2.executeUpdate();
    }

    private void saveIfNaturalEntity(BusinessPartner p, PreparedStatement stmt3) throws SQLException {
        if (!(p instanceof NaturalEntity))
            return;
        NaturalEntity temp = (NaturalEntity) p;
        stmt3.setString(1, temp.getPersonalID());
        stmt3.setString(2, temp.getFirstname());
        stmt3.setString(3, temp.getLastname());
        stmt3.setInt(4, temp.getId());

        stmt3.executeUpdate();
    }

    public void saveOrUpdate(BusinessPartner partner) throws SQLException {
        String select = "select * from business_partner;";
        PreparedStatement stmt = connection.prepareStatement(select, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            /*u slucaju da taj partner vec postoji u bazi, tj. vec mu je izgenerisan id*/
            if (partner.getId() != 0 && rs.getInt("id") == partner.getId()) {
                rs.updateString("name", partner.getName());
                rs.updateInt("address_id", partner.getAddress().getId()); // pretpostavka je da adrese moraju vec da postoje u bazi
                updateIfNaturalEntity(partner);
                updateIfLegalEntity(partner);
                rs.updateRow(); // cuvanje izmena u redu u tabeli business_partner
                System.out.println("Updated old partner");
                return;
            }
        }

        /*u slucaju da dati partner ne postoji u bazi*/
        rs.moveToInsertRow();
        rs.updateString("name", partner.getName());
        rs.updateInt("address_id", partner.getAddress().getId());
        rs.updateInt("id", 100);
        rs.insertRow();
        rs.close();
        partner.setId(100);
        /*unos u tabele konkretnih klasa*/
        PreparedStatement insertLegalEntity =
                connection.prepareStatement("insert into legal_entity (lei, founding_year, id) values (?, ?, ?)");
        PreparedStatement insertNaturalEntity =
                connection.prepareStatement("insert into natural_entity (personal_id, firstname, lastname, id) values (?, ?, ?, ?)");
        saveIfLegalEntity(partner, insertLegalEntity);
        saveIfNaturalEntity(partner, insertNaturalEntity);
        System.out.println("Saved new partner");
    }

    private void updateIfNaturalEntity(BusinessPartner partner) throws SQLException {
        if (!(partner instanceof NaturalEntity)) return;
        NaturalEntity e = (NaturalEntity) partner;
        Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet rs = statement.executeQuery("select * from natural_entity where id = " + partner.getId() + ";");
        if (rs.next()){
            rs.updateString("personal_id", e.getPersonalID());
            rs.updateString("firstname", e.getFirstname());
            rs.updateString("lastname", e.getLastname());
            rs.updateRow();// cuvanje izmena u redu u tabeli natural_entity
        }
    }

    private void updateIfLegalEntity(BusinessPartner partner) throws SQLException {
        if (!(partner instanceof LegalEntity)) return;
        LegalEntity e = (LegalEntity) partner;

        Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet rs = statement.executeQuery("select * from legal_entity where id = " + partner.getId() + ";");
        if (rs.next()){
            rs.updateString("lei", e.getLEI());
            rs.updateInt("founding_year", e.getFoundingYear());
            rs.updateRow(); // cuvanje izmena u redu u tabeli legal_entity
        }
    }

    public void saveAddresses(List<Address> addresses) throws SQLException {
        String sql = "insert into address (city, street, number) values (?, ?, ?)";
        PreparedStatement stmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
        for (Address a : addresses) {
            stmt.setString(1, a.getCity());
            stmt.setString(2, a.getStreet());
            stmt.setString(3, a.getNumber());

            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            a.setId(rs.getInt(1));
            rs.close();
        }
        stmt.close();
    }

    public void commit() {
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void rollback() {
        try {
            connection.rollback();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
