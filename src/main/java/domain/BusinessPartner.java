package domain;

public abstract class BusinessPartner {
    protected int id;
    protected String name;
    protected Address address;

    public BusinessPartner(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    public BusinessPartner() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
