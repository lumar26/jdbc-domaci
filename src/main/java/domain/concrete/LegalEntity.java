package domain.concrete;

import domain.Address;
import domain.BusinessPartner;

public class LegalEntity extends BusinessPartner {
    private String LEI;
    private int foundingYear;

    public LegalEntity(String name, Address address, String LEI, int foundingYear) {
        super(name, address);
        this.LEI = LEI;
        this.foundingYear = foundingYear;
    }

    public LegalEntity() {
    }

    public int getFoundingYear() {
        return foundingYear;
    }

    public void setFoundingYear(int foundingYear) {
        this.foundingYear = foundingYear;
    }

    public String getLEI() {
        return LEI;
    }

    public void setLEI(String LEI) {
        this.LEI = LEI;
    }
}
