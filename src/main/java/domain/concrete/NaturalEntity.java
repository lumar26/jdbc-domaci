package domain.concrete;

import domain.Address;
import domain.BusinessPartner;

public class NaturalEntity extends BusinessPartner {
    private String personalID;
    private String firstname;
    private String lastname;

    public NaturalEntity(String name, Address address, String personalID, String firstname, String lastname) {
        super(name, address);
        this.personalID = personalID;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public NaturalEntity() {
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPersonalID() {
        return personalID;
    }

    public void setPersonalID(String personalID) {
        this.personalID = personalID;
    }
}
