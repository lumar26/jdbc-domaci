package main;

import domain.Address;
import domain.BusinessPartner;
import domain.concrete.LegalEntity;
import domain.concrete.NaturalEntity;
import service.Service;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Address address1 = new Address("Beograd", "Niksicka", "6/2/8");
        Address address2 = new Address("Krezbinac", "Djure Jaksica", "3");
        Address address3 = new Address("Paracin", "Tome Zivanovica", "20");

        ArrayList<Address> addresses = new ArrayList<>();
        addresses.add(address1);
        addresses.add(address2);
        addresses.add(address3);

        BusinessPartner bp1 = new NaturalEntity("lumar", address2, "2602000723212", "Luka", "Marinkovic");
        BusinessPartner bp2 = new NaturalEntity("lamar", address2, "2908995723212", "Lazar", "Marinkovic");
        BusinessPartner bp3 = new LegalEntity("Other Deloitte", address2, "22222222", 1987);
        BusinessPartner bp4 = new LegalEntity("Siemens", address2, "4321", 1960);

        List<BusinessPartner> partners = new ArrayList<>();
        partners.add(bp1);
        partners.add(bp2);
        partners.add(bp3);

        Service service = new Service();
        try {
            service.saveAddresses(addresses);
            service.saveBusinessPartners(partners);
            service.saveOrUpdatePartner(bp3);
            service.saveOrUpdatePartner(bp4);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
